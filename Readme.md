# Kevin's VOIP blog

This site uses Jekyll to build the published version, and can be generated
manually/locally using something like this:
```
bundle install
cd voip-blog
jekyll build
```

This uses Gitlab pages to publish the site, which is available at
[https://klschaefer.gitlab.io/voip-blog/]
