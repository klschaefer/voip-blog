#!/bin/bash

echo "** New post generator **"
echo -n "  Title alias for url (eg: some-title):"
read url_title

echo -n "  Display title (eg: Some Title):"
read title

# Create the date to use in the blog post itself
formatted_date=$(date "+%Y-%m-%d %H:%M:%S %z")

# Create the date for the filename
file_date=$(date "+%Y-%m-%d")

# Create the filename
filename="voip-blog/_posts/${file_date}-${url_title}.md"

# Create the file itself
cat > $filename <<EOF
---
layout: post
title:  "$title"
date:   $formatted_date
---

EOF

echo Created blank blog post file at $filename
