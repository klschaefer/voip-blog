---
layout: post
title:  "Phone Provisioning"
date:   2018-02-03 12:10:00 -0600
---
Provisioning allows phones to be automatically set up to work with the PBX. Provisioning
uses TFTP to store and serve configuration files that phones will automatically apply. DHCP
is used to tell phones where to find the TFTP server to use for provisioning.

First, you'll need to log on to your DHCP server to set up the necessary DHCP option. For Cisco
phones, you can use Option 150 (the Cisco proprietary option which supports an array of servers)
or Option 66 (the standard).

## Refs
<http://blog.router-switch.com/2013/03/dhcp-option-150-dhcp-option-66/>
